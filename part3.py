"""Extra runs concede per team in 2016"""
import csv
import matplotlib.pyplot as plt
import get_match_ids


def execute(match_id_season):
    """Extracts data from matches and deliveries csv for plotting"""
    with open("deliveries.csv") as deliveries_file:
        delivery_stats_reader = csv.DictReader(deliveries_file)

        runs_conceded = {}
        for delivery in delivery_stats_reader:
            match_id = delivery['match_id']
            bowling_team = delivery['bowling_team']
            extra_runs = int(delivery['extra_runs'])
            if match_id in match_id_season:
                if bowling_team in runs_conceded:
                    runs_conceded[bowling_team] += extra_runs
                else:
                    runs_conceded[bowling_team] = 0
    plot(runs_conceded)


def plot(runs_conceded):
    """Plots data obtained from execute function"""
    team, extra_runs = zip(*runs_conceded.items())
    plt.bar(team, extra_runs)
    plt.xlabel("Teams")
    plt.xticks(rotation=-90)
    plt.ylabel("Extra Runs Conceded")
    plt.title("Extra Runs Conceded per Team in 2016")
    plt.show()


execute(get_match_ids.ids_of_matches_played("2016"))
