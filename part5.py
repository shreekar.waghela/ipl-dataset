"""Top wicket tackers in 2015"""
import csv
import matplotlib.pyplot as plt


def execute(delivery_file):
    """Extracts data from deliveries csv for calculation"""
    with open(delivery_file) as deliveries_file:
        delivery_stats_reader = csv.DictReader(deliveries_file)

        bowler_stats = {}
        for delivery in delivery_stats_reader:
            bowler = delivery['bowler']
            dismissal = delivery['player_dismissed']
            dismissal_type = delivery['dismissal_kind']

            if dismissal != '':
                if dismissal_type != 'run out':
                    if bowler in bowler_stats:
                        bowler_stats[bowler] += 1
                    else:
                        bowler_stats[bowler] = 1
    calculate(bowler_stats)


def calculate(stats):
    """Calculates wickets taken by bowlers and gives top 10"""
    wickets = []
    for bowler, wicket in stats.items():
        wickets.append(wicket)
    wickets.sort(reverse=True)
    top_bowlers = []
    top_wickets = []
    for wicket_value in wickets[:10]:
        for bowler, wicket in stats.items():
            if wicket_value == wicket:
                top_bowlers.append(bowler)
                top_wickets.append(wicket)
    plot(top_bowlers, top_wickets)


def plot(bowler, wickets):
    """Plots top 10 wicket takers in IPL"""
    plt.bar(bowler, wickets)
    plt.xlabel("Bowlers")
    plt.ylabel("Wickets")
    plt.title("Top Wicket Takers")
    plt.show()


execute("deliveries.csv")
