"""Importing match ids for particular year"""
import csv 

def ids_of_matches_played(year):
    match_ids = []    
    with open("matches.csv") as matches_file:
            match_stats_reader = csv.DictReader(matches_file)
            for match in match_stats_reader:
                if match['season'] == year:
                    match_ids.append(match['id'])
    return match_ids
