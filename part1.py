"""Number of matches played per year of IPL (all seasons)"""
import csv
import matplotlib.pyplot as plt

def extract(file_path):
    """ Extracts data from csv file and obtains data for plotting"""
    with open(file_path) as matches_file:
        match_stats_reader = csv.DictReader(matches_file)

        matches_played = {}
        for match in match_stats_reader:
            if match['season'] in matches_played:
                matches_played[match['season']] += 1
            else:
                matches_played[match['season']] = 1
    return matches_played


def plot(matches_details):
    """Plots the data obtained from extract function"""
    matches_played_list = sorted(matches_details.items())
    year, matches = zip(*matches_played_list)

    plt.bar(year, matches)
    plt.xlabel('Years')
    plt.ylabel('Matches Played')
    plt.title('Matches Played Per Year')
    plt.show()


plot(extract("matches.csv"))
