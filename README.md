# IPL DATA SET

This project is about analyzing IPL dataset from https://www.kaggle.com/manasgarg/ipl

There are five parts in this project.

## Part-1:
This part plots the total number of matches played in all years.
![Part1](./Figure_1.png)

## Part-2:
This part plots a stacked bar chart of matches won by each team in all the years.
![Part2](./Figure_2.png)

## Part-3:
This part plots the extra runs conceded by each team in year 2016.
![Part3](./Figure_3.png)

## Part-4:
This part plots the top economical bowlers in the year 2015.
![Part4](./Figure_4.png)

## Part-5:
This part plots the highest wicket takers in IPL history.
![Part5](./Figure_5.png)
