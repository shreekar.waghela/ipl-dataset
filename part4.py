"""Top Economical Bowlers in 2015"""
import csv
import matplotlib.pyplot as plt
import get_match_ids


def execute(match_ids_season):
    """Extracts data from deliveries csv for calculating"""
    with open("deliveries.csv") as deliveries_file:
        delivery_stats_reader = csv.DictReader(deliveries_file)

        bowler_stats = {}
        for delivery in delivery_stats_reader:
            bowler = delivery['bowler']
            wide_runs = int(delivery['wide_runs'])
            noball_runs = int(delivery['noball_runs'])
            batsman_runs = int(delivery['batsman_runs'])
            runs_per_ball = wide_runs + noball_runs + batsman_runs

            if delivery['match_id'] in match_ids_season:
                if delivery['is_super_over'] == '0':
                    if bowler in bowler_stats:
                        bowler_stats[bowler]['runs_conceded'] += runs_per_ball
                        bowler_stats[bowler]['balls'] += 1
                    else:
                        bowler_stats[bowler] = {
                            'runs_conceded': runs_per_ball, 'balls': 1}
                    if wide_runs != 0 or noball_runs != 0:
                        bowler_stats[bowler]['balls'] -= 1
    calculate(bowler_stats)


def calculate(bowler_stats):
    """Calculates economy of bowler based on data obtained from execute function"""
    bowler_economy = {}
    for bowler in bowler_stats:
        economy = (bowler_stats[bowler]['runs_conceded']
                   )/(bowler_stats[bowler]['balls']/6)
        bowler_economy[bowler] = round(economy, 2)
    corresponding_bowler_economy = []
    for bowler, economy in bowler_economy.items():
        corresponding_bowler_economy.append(economy)

    corresponding_bowler_economy.sort()
    top_bowlers = []
    top_economy = []
    for economy_value in corresponding_bowler_economy[:10]:
        for bowler, economy in bowler_economy.items():
            if economy_value == economy:
                top_bowlers.append(bowler)
                top_economy.append(economy)
    plot(top_bowlers, top_economy)


def plot(top_bowlers, top_economy):
    """Plots data obtained from calculate function"""
    plt.bar(top_bowlers, top_economy)
    plt.xlabel("Bowlers")
    plt.ylabel("Economy")
    plt.title("Top Economical Bowlers")
    plt.show()


execute(get_match_ids.ids_of_matches_played("2015"))
