"""Matches won by every team in every year"""
import csv
import matplotlib.pyplot as plt


def execute(file_path):
    """Extracts data from matches csv for plotting"""
    with open(file_path) as match_file:
        match_stats_reader = csv.DictReader(match_file)

        match_wins = {}
        match_year_array = []
        for match in match_stats_reader:
            match_dict = dict(match)
            winner = match_dict['winner']
            season = match_dict['season']
            if winner in match_wins:
                if season in match_wins[winner]:
                    match_wins[winner][season] += 1
                else:
                    match_wins[winner][season] = 1
            else:
                match_wins[winner] = {}
            match_year_array.append(season)
    for team in match_wins:
        for year in set(match_year_array):
            if year not in match_wins[team]:
                match_wins[team][year] = 0
    transform(match_wins, match_year_array)


def transform(match_wins_dict, match_year_array):
    """Transforming data after extracting from csv file"""
    match_years = list(set(match_year_array))
    match_years.sort()
    winner_teams = []
    wins_per_year = []

    for team, year_dict in match_wins_dict.items():
        winner_teams.append(team)
        wins_per_year.append(dict(sorted(year_dict.items())))

    wins_per_year_final = []
    for win_data in wins_per_year:
        wins_per_year_final.append(list(win_data.values()))
    plot(wins_per_year_final, match_years, winner_teams)


def plot(wins, years, teams):
    """Plots data obtained from execute function"""
    bottom_point = [0]*10
    for team_wins in wins:
        team_label = teams[wins.index(team_wins)]
        plt.bar(years, team_wins, width=0.7,
                bottom=bottom_point, label=team_label)
        bottom_point = [
            win+point for (win, point) in zip(team_wins, bottom_point)]
    plt.xlabel("Season Years")
    plt.ylabel("Number of Matches Won by each Team")
    plt.title("Matches Won by Teams per season")
    plt.legend(bbox_to_anchor=(1, 1), ncol=5, loc=4, borderaxespad=1.5)
    plt.show()


execute("matches.csv")
